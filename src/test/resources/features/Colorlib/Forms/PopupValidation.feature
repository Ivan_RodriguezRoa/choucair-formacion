#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

@Regresion
Feature: Validacion del Popup
  El usuario debe ingresar al formulario todos los datos requeridos.
  Estos campos realizar varias validaciones que son oblogatorias.

  @CasoExitoso
  Scenario: Diligenciamento valido del formulario Popup
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad Forms Validation
    When Diligencio Formulario Popup Validation
    | Required | Select | MultipleS1 | MultipleS2 | Url                   | email            | Password1 | Password2 | Minsize | Maxsize | Number | IP          | Date       | DataEarlier|
    | Valor1   | Golf   | Tennis     | Golf       | http://www.valor1.com | Valor1@gmail.com | valor1    | valor1    | 123456  | 123456  | -99.99 | 200.200.5.4 | 2018-01-22 | 2012/09/12 |
   	Then Verifico ingreso exitoso
    
  @CasoAlterno
  Scenario: Diligenciamento valido del formulario Popup
  					Se presenta Globo Informativo indicando error en el diligenciamento de algunos campos
    Given Autentico en colorlib con usuario "demo" y clave "demo"
    And Ingreso a la funcionalidad Forms Validation
    When Diligencio Formulario Popup Validation
    | Required | Select | MultipleS1 | MultipleS2 | Url                   | email            | Password1 | Password2 | Minsize | Maxsize | Number | IP          | Date       | DataEarlier|
    |          | Golf   | Tennis     | Golf       | http://www.valor1.com | Valor1@gmail.com | valor1    | valor1    | 123456  | 123456  | -99.99 | 200.200.5.4 | 2018-01-22 | 2012/09/12 |
    | Valor1   |Choose a sport   | Tennis     | Golf       | http://www.valor1.com | Valor1@gmail.com | valor1    | valor1    | 123456  | 123456  | -99.99 | 200.200.5.4 | 2018-01-22 | 2012/09/12 |
   	Then Verificar que se presente Globo Informativo de Validación

 # @tag2
  #Scenario Outline: Title of your scenario outline
   # Given I want to write a step with <name>
    #When I check for the <value> in step
    #Then I verify the <status> in step

    #Examples: 
     # | name  | value | status  |
      #| name1 |     5 | success |
      #| name2 |     7 | Fail    |
